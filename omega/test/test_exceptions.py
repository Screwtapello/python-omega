import unittest
from omega import exceptions


class TestLineAndColumn(unittest.TestCase):

	def test_pos_0(self):
		self.assertEqual(
			exceptions._line_and_column("ab\ncd", 0),
			(1, 0),
		)

	def test_pos_1(self):
		self.assertEqual(
			exceptions._line_and_column("ab\ncd", 1),
			(1, 1),
		)

	def test_pos_2(self):
		self.assertEqual(
			exceptions._line_and_column("ab\ncd", 2),
			(1, 2),
		)

	def test_pos_3(self):
		self.assertEqual(
			exceptions._line_and_column("ab\ncd", 3),
			(2, 0),
		)

	def test_pos_4(self):
		self.assertEqual(
			exceptions._line_and_column("ab\ncd", 4),
			(2, 1),
		)

	def test_pos_5(self):
		self.assertEqual(
			exceptions._line_and_column("ab\ncd", 5),
			(2, 2),
		)
