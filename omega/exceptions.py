#!/usr/bin/python
"""
Exceptions that can be raised by Omega parser instances.
"""


def _line_and_column(sequence, pos):
	line_start = sequence.rfind('\n', 0, pos) + 1
	col = pos - line_start
	line = sequence.count('\n', 0, line_start) + 1

	return line, col


class ParseError(Exception):
	"""
	Parent of all possible parsing problem.
	"""
	def __init__(self, sequence, pos, got, expected):
		if isinstance(sequence, str):
			line, col = _line_and_column(sequence, pos)
			message = "%s:%s:Got %s, expected %s" % (line, col, got, expected)

		else:
			message = "%s: Got %s, expected %s" % (pos, got, expected)

		super(ParseError, self).__init__(message)

		self.sequence = sequence
		self.pos = pos
		self.got = got
		self.expected = expected


class UnrecoverableParseError(ParseError):
    """
    A parsing problem that can't be solved by back-tracking.
    """


class BacktrackException(ParseError):
    """
    A parsing problem that might be solved by back-tracking.
    """


