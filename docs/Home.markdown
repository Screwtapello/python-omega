Documentation Home
==================

Here is an overview of the documentation available for Omega:

- [Tutorial](Tutorial): Getting started with Omega and creating your first
  parser.
- Omega [Language Reference](Language): A guide to all the language features in
  Omega, with examples.
- Omega [Library Reference](Library): A guide to the parsing-rules built-in to
  Omega, with examples.
- Python [API Reference](API): Details on how to use Omega parsers from
  Python code, and creating hybrid Omega/Python parsers.
